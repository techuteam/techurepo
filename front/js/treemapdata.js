google.charts.load('current', {'packages':['treemap']});
google.charts.setOnLoadCallback(drawChart);

function getBalanceTree(idClient, balanceTree ) {
    var request = new XMLHttpRequest();
    var datos = { "idclient": idClient };
    var expensesAmount=0;
    var incomeAmount=0;


    // Adding categories to the trees
    var request = new XMLHttpRequest();
    request.open("POST", "http://servidorback:3000/transactionCategories", false);
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(datos));
    if (request.responseText == null) {
      console.log("Sin respuesta");
    } else {
      //console.log(request.responseText);
      var categories= JSON.parse(request.responseText);
    }
    for (var i=0; i<categories.length; i++) {
      if (categories[i].category == "" || categories[i].category == null ) { categories[i].category = 'Sin clasificar'}
      if (categories[i].amountExpenses>0) {
        var row = [categories[i].category+ " (-)", 'Expenses', Math.round(categories[i].amountExpenses*100)/100];
        expensesAmount+=categories[i].amountExpenses;
        balanceTree.push(row);
      }
      if (categories[i].amountIncome>0) {
        var row = [categories[i].category+ " (+)", 'Income', Math.round(categories[i].amountIncome*100)/100];
        incomeAmount+=categories[i].amountIncome;
        balanceTree.push(row);
      }
    }

    // Adding main category.
    balanceTree.push(['Balance', null, incomeAmount-expensesAmount]);
    balanceTree.push(['Expenses', 'Balance', Math.round(expensesAmount*100)/100]);
    balanceTree.push(['Income', 'Balance', Math.round(incomeAmount*100)/100]);


    // Adding transactions in each tree income/expenses
    request.open("POST", "http://servidorback:3000/transactions", false);
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(datos));
    var ids = JSON.parse(request.responseText);

    //console.log("Vienen "+ids.length +" items");
    for (var i=0; i<ids.length; i++) {
      if (ids[i].category == null  || ids[i].category == "" ) { ids[i].category="Sin clasificar";}
      if (ids[i].amount < 0) {
        var row = [ids[i].date+" "+ids[i].item+" "+ids[i].amount+"\n "+ids[i]._id, ids[i].category+ " (-)", -ids[i].amount];
        balanceTree.push(row);
      } else {
        var row = [ids[i].date+" "+ids[i].item+" "+ids[i].amount+"\n "+ids[i]._id, ids[i].category+ " (+)", ids[i].amount];
        balanceTree.push(row);
      }
    }
    console.log("balanceTree: " + JSON.stringify(balanceTree) );
}

function drawChart() {
  // array for data
  var balanceTree=[];

  // recover param
  var idclient = document.baseURI.split('?')[1] ;

  // user must be logged
  var usr = sessionStorage.getItem("usuario", this.usuario);
  if ( usr == null) {
    document.getElementById('chart_div').innerHTML = "Operación no permitida";
    console.log("Operación no permitida");
    return(0);
  }

  // Recover client data
  getBalanceTree(idclient, balanceTree);

  // Fill goole tree object
  var balance = new google.visualization.DataTable();
  balance.addColumn('string', 'ID');
  balance.addColumn('string', 'Parent');
  balance.addColumn('number', 'Importe');
  balance.addRows(balanceTree);

  // built de graph
  var treeBalance = new google.visualization.TreeMap(document.getElementById('chart_div'));

  // set the options graph
  var optionsbalance = {
    highlightOnMouseOver: true,
    maxDepth: 1,
    maxPostDepth: 2,
    minHighlightColor: '#8c6bb1',
    midHighlightColor: '#9ebcda',
    maxHighlightColor: '#edf8fb',
    minColor: '#009688',
    midColor: '#f7f7f7',
    maxColor: '#ee8100',
    headerHeight: 15,
    showScale: true,
    height: 500,
    useWeightedAverageForAggregation: true,
    generateTooltip: showTooltipBalance
  };

  // draw graph
  treeBalance.draw(balance, optionsbalance);

  // show ToolTip.
  function showTooltipBalance(row, size, value) {
    return '<div style="background:#fd9; padding:10px; border-style:solid">' +
           '<span style="font-family:Courier"><b>' + balance.getValue(row, 0) +
           '</b>, ' + balance.getValue(row, 2) + ' </div>';
  }
}

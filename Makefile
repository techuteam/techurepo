all: front.run back.run

install: all.clean all.run

all.run: postgresql.run mongo.run back.run front.run

all.clean: 
	./psclean for
	./imgclean for
	systemctl stop mongodb || echo "mongo no activo"
	systemctl stop postgresql || echo "sqlpostgre no activo"

clean: ps.clean img.clean 

status:
	sudo docker images
	sudo docker ps
	sudo docker network ls

front.run: front.docker
	sudo docker run -p 2999:3000 --net redtechu --name servidorfront -d foroga/front

front.docker :
	cd front && polymer build
	sudo docker build -t foroga/front front

back.run: back.docker
	sudo docker run -p 3000:3000 --net redtechu --name servidorback -d foroga/back

back.docker :
	sudo docker build -t foroga/back back

mongo.run: mongo.docker
	sudo docker run -v ${PWD}/mongo/datosmongo:/data/db -p 27017:27017 --name srvmongo --net redtechu for/mongo &
	sleep 3
	mongoimport -d local -c categories --type csv --file mongo/categorias.csv --headerline --drop
	mongoimport -d local -c transactions --type csv --file mongo/transactions.csv --headerline --drop

mongo.docker: mongo/Dockerfile
	sudo docker build -t for/mongo mongo

postgresql.run: postgresql.docker
	sudo docker run -p 5432:5432  --net redtechu --name srvpostgresql -d for/postgresql
	sleep 3
	cat postgreSQL/usuarios.sql | psql -h srvpostgresql -p 5432 -U docker

postgresql.docker: postgreSQL/Dockerfile
	sudo docker build -t for/postgresql postgreSQL

net.mk:
	sudo docker network create redtechu

net.rm:
	sudo docker network rm redtechu

ps.clean :
	./psclean foroga

img.clean:
	./imgclean foroga

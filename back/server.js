var express = require('express'),
  app = express(),
  cors = require('cors'),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

var path = require('path');

var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://srvmongo:27017/local";

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=50c5ea68e4b0a97d668bc84a";

/*
var urlLogin = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=50c5ea68e4b0a97d668bc84a";
var login = requestjson.createClient(urlLogin);
*/

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.post('/movimientos2', function(req, res) {
   mongoClient.connect(url, function(err, db) {
      if(err) {
        console.log(err);
      } else  {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        var id =  parseInt(req.body.idcliente);
        if (req.body.pagina==null) {
          col.find({ "idcliente": id}).limit(10000).toArray(function(err, docs) {
            res.send(docs);
          });
        } else {
          var pag = parseInt(req.body.pagina);
          col.find({ "idcliente": id}).skip(pag*8).limit(8).toArray(function(err, docs) {
            res.send(docs);
          });
        }
        db.close();
     }
  })
});


//
// get the transactions for one client.
// params: client , (page)
//
app.post('/transactions', function(req, res) {
   mongoClient.connect(url, function(err, db) {
      if(err) {
        console.log(err);
      } else  {
        var col = db.collection('transactions');
        var id =  parseInt(req.body.idclient);
        console.log("transactions.idclient: ", id);
        if (req.body.page==null) {
          col.find({ "idclient": id}).limit(10000).toArray(function(err, docs) {
            res.send(docs);
          });
        } else {
          var pag = parseInt(req.body.page);
          col.find({ "idclient": id}).skip(pag*8).limit(8).toArray(function(err, docs) {
            res.send(docs);
          });
        }
        db.close();
     }
  })
});

// Obtener movimientos del cliente idcliente desde MLab
app.post('/transaction', function(req, res) {
   mongoClient.connect(url, function(err, db) {
      if(err) {
        console.log(err);
      } else  {
        console.log("Connected successfully to server");
        var col = db.collection('transactions');
        var _id =  req.body.idMovimiento;
        var category = req.body.categoria;
        var ObjectID = require('mongodb').ObjectID;
        console.log("idMovimimiento: "+_id+ " Categoria:" + category);
        col.update({ "_id" : ObjectID(_id) }, { $set: {"category":category} }, function(err, docs) {
          res.send(docs);
        });
        db.close();
     }
  })
});

app.get('/categories', function(req, res) {
   mongoClient.connect(url, function(err, db) {
      if(err) {
        console.log(err);
      } else  {
        var col = db.collection('categories');
        console.log("Connected successfully to server");
        col.find({}).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
     }
  })
});

// Total Amount of transactions of one client group by category
//
app.post('/transactionCategories', function(req, res) {
  mongoClient.connect(url, function(err, db) {
     if(err) {
       console.log(err);
     } else  {
       console.log("Connected successfully to server");
       var col = db.collection('transactions');
       var id =  parseInt(req.body.idclient);
       console.log("id " + id);
       col.group( ["category"],
                  {"idclient": id},
                  { "amountIncome": 0,
                    "amountExpenses": 0 },
                  "function(obj, prev) { if (obj.amount>0) prev.amountIncome += obj.amount; if (obj.amount<0) prev.amountExpenses -= obj.amount; }",
                  function(err, docs) {
                   res.send(docs);
                  });
       db.close();
    }
  })
});


// Cambios para postgresql
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@srvpostgresql:5432/bdseguridad";
app.post('/login', function(req, res) {
  var clientepostgre = new pg.Client(urlUsuarios);
  clientepostgre.connect();
  console.log("Hola "+ req.body.usuario+ " "+ req.body.password);
  const query = clientepostgre.query(
    'select idcliente from usuarios where usuario=$1 and password=$2',
    [req.body.usuario, req.body.password],
     (err,result) => {
       if (err) {
         console.log(err);
         res.send(err);
       } else {
         if (result.rowCount == 1) {
           res.send(""+result.rows[0].idcliente);
         } else {
           res.send("0");
         }
       }
       clientepostgre.end();
   });
});

app.get('/movimientos', function(req, res) {
  var clienteMlab = requestjson.createClient(urlmovimientosMlab);
  clienteMlab.get("", function(err, resM, body) {
    if(err)
    {
      console.log(err);
    }
    else
    {
      res.send(body);
    }
  });
});


app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  var clienteMlab = requestjson.createClient(urlmovimientosMlab
    + "&f={'idcliente':1, 'nombre': 1, 'apellidos': 1}"
    + "?apiKey=50c5ea68e4b0a97d668bc84a" );
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

/*
** Código no necesario. Solo para consulta
**
app.post('/movimientos2', function(req, res) {
  mongoClient.connect(url, function(err, db) {
    var col = db.collection('movimientos');
    console.log("Traying to intert to server");
    db.collection('movimientos').insertOne({a:1}, function(err, r) {
      console.log(r.insertedCount + ' registros insertados');
    });

    db.collection('movimientos').insertMany({a:1}, {a:3}], function(err, r) {
      console.log(r.insertedCount + ' registros insertados');
    });

    db.collection('movimientos').insertOne(req.body, functbackion(err, r) {
      console.log(r.insertedCount + ' registros insertados');
    });

    db.close();
    res.send("ok");
  })
});

// Crear movimiento en MLab
app.post('/movimientos', function(req, res) {
});

// Obtener movimientos del cliente idcliente desde MLab
app.get('/movimientos/:idcliente', function (req, res) {
})

// Obtener movimientos del cliente idcliente desde MLab
app.post('/movimiento', function(req, res) {
   mongoClient.connect(url, function(err, db) {
      if(err) {
        console.log(err);
      } else  {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        var _id =  req.body.idMovimiento;
        var categoria = req.body.categoria;
        var ObjectID = require('mongodb').ObjectID;
        console.log("idMovimimiento"+_id+ " Categoria:" + categoria);
        col.update({ "_id" : ObjectID(_id) }, { $set: {"categoria":categoria} }, function(err, docs) {
          res.send(docs);
        });
        db.close();
     }
  })
});

app.post('/transactionItems', function(req, res) {
  mongoClient.connect(url, function(err, db) {
     if(err) {
       console.log(err);
     } else  {
       console.log("Connected successfully to server");
       var col = db.collection('transactions');
       var id =  parseInt(req.body.idclient);
       console.log("id " + id);
       col.group( ["item", "category"],
                  {"idclient": 2},
                  { "count": 0 },
                  "function(obj, prev) { prev.count += 1; }",
                  function(err, docs) {
                   res.send(docs);
                  });
       db.close();
    }
  })
});

app.post('/transactionIds', function(req, res) {
  mongoClient.connect(url, function(err, db) {
     if(err) {
       console.log(err);
     } else  {
       console.log("Connected successfully to server");
       var col = db.collection('transactions');
       var id =  parseInt(req.body.idclient);
       console.log("id " + id);
       col.group( ["_id", "item", "date", "category"],
                  {"idclient": 2},
                  { "amount": 0 },
                  "function(obj, prev) { prev.amount+= obj.amount; }",
                  function(err, docs) {
                   res.send(docs);
                  });
       db.close();
    }
  })
});


app.post('/login1', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    console.log(req.body);
    var usuario = req.body.usuario;
    var password = req.body.password;
    console.log(usuario);
    console.log(password);
    if ((usuario == 'Oscar') && (password == 'Rey')) {
      res.send("Cliente valido");
    } else {
      res.send("Cliente invalido");
    }

});

*/
